//
//  UIDirectoryViewController.h
//  UITableViewNavigation
//
//  Created by Sergey on 15/09/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDirectoryViewController : UITableViewController


- (id) initWithFolderPath:(NSString*) path;
@end
