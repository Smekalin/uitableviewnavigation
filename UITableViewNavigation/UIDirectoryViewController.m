//
//  UIDirectoryViewController.m
//  UITableViewNavigation
//
//  Created by Sergey on 15/09/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import "UIDirectoryViewController.h"

@interface UIDirectoryViewController ()

@property (strong, nonatomic) NSString* path;
@property (strong, nonatomic) NSArray* contents;
@end

@implementation UIDirectoryViewController

- (id) initWithFolderPath:(NSString*) path {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        NSError* error = nil;
        self.path = path;
        _contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_path error:&error];
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [_path lastPathComponent];
    
    if ([self.navigationController.viewControllers count] > 1) {
        UIBarButtonItem* rootButton = [[UIBarButtonItem alloc] initWithTitle:@"Root"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(actionRoot:)];
        self.navigationItem.rightBarButtonItem = rootButton;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"path = %@", _path);
    NSLog(@"vc count = %ld", [self.navigationController.viewControllers count]);
    NSLog(@"vc count = %ld", [self.navigationController.viewControllers count]);
    NSLog(@"index self = %ld", [self.navigationController.viewControllers indexOfObject:self]);
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_contents count];
}

- (BOOL) isDirectoryAtIndexPath:(NSIndexPath*) indexPath {
    
    NSString* fileName = [_contents objectAtIndex:indexPath.row];
    
    NSString* filePath = [_path stringByAppendingPathComponent:fileName];
    
    BOOL isDirectory = NO;
    
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    return isDirectory;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if ([self isDirectoryAtIndexPath:indexPath]) {
        cell.imageView.image = [UIImage imageNamed:@"folder.png"];
    }
    else {
        cell.imageView.image = [UIImage imageNamed:@"file.png"];
    }

    cell.textLabel.text = [_contents objectAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self isDirectoryAtIndexPath:indexPath]) {
        NSString* fileName = [_contents objectAtIndex:indexPath.row];
        
        NSString* path = [_path stringByAppendingPathComponent:fileName];
        UIDirectoryViewController* dc = [[UIDirectoryViewController alloc] initWithFolderPath:path];
        [self.navigationController pushViewController:dc animated:YES];
    }
}

#pragma mark - Actions

- (void)actionRoot:(UIBarButtonItem*) sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
